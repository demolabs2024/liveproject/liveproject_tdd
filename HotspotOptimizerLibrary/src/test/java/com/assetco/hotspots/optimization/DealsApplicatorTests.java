package com.assetco.hotspots.optimization;

import com.assetco.search.results.*;
import org.junit.jupiter.api.*;

import static org.mockito.Mockito.*;

public class DealsApplicatorTests {
    protected AssetAssessments assessments;
    protected AssetMeasurements measurements;
    protected DealsApplicator applicator;
    protected Asset asset;
    protected SearchResults searchResults;

    protected void setGoverningRelationshipLevel(DealsApplicator applicator) {
        this.applicator = applicator;
    }

    public void baseSetup() {
        assessments = mock(AssetAssessments.class);
        measurements = mock(RevenueBasedAssetMeasurements.class);
        searchResults = new SearchResults();
    }

    protected void givenAssetVendorLevel(AssetVendorRelationshipLevel basic) {
        asset = AssetInfoFactory.getAsset(basic);
    }

    protected void thenAssetIsInDeals(boolean expected) {
        Assertions.assertEquals(expected, searchResults.getHotspot(HotspotKey.Deals).getMembers().contains(asset));
    }

    protected void whenApplyDeals() {
        applicator.applyDeals(searchResults, asset);
    }

    protected void givenDealEligible(boolean value) {
        when(assessments.isAssetDealEligible(asset)).thenReturn(value);
    }

    protected void givenPerformance(ActivityLevel value) {
        when(measurements.getAssetPerformance(asset)).thenReturn(value);
    }

    protected void givenVolume(ActivityLevel value) {
        when(measurements.getAssetVolume(asset)).thenReturn(value);
    }
}
