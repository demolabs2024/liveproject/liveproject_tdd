package com.assetco.hotspots.optimization;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.assetco.hotspots.optimization.ActivityLevel.*;
import static com.assetco.hotspots.optimization.AssetInfoFactory.getAssetWith24HourTraffic;

public class TrafficBasedAssetMeasurementsTests extends AssetMeasurementsTests{
    @BeforeEach
    public void setup() {
        measurements = new TrafficBasedAssetMeasurements();
    }

    @Test
    public void minimumVolume() {
        givenTotalSold(0);

        whenMeasureVolume();

        thenMeasuredActivityLevelIs(Insignificant);
    }

    @Test
    public void maximumInsignificantVolume() {
        givenTotalSold(9);

        whenMeasureVolume();

        thenMeasuredActivityLevelIs(Insignificant);
    }

    @Test
    public void minimumElevatedVolume() {
        givenTotalSold(10);

        whenMeasureVolume();

        thenMeasuredActivityLevelIs(Elevated);
    }

    @Test
    public void maximumElevatedVolume() {
        givenTotalSold(19);

        whenMeasureVolume();

        thenMeasuredActivityLevelIs(Elevated);
    }

    @Test
    public void minimumHighVolume() {
        givenTotalSold(20);

        whenMeasureVolume();

        thenMeasuredActivityLevelIs(High);
    }

    @Test
    public void maximumHighVolume() {
        givenTotalSold(99);

        whenMeasureVolume();

        thenMeasuredActivityLevelIs(High);
    }

    @Test
    public void minimumExtremeVolume() {
        givenTotalSold(100);

        whenMeasureVolume();

        thenMeasuredActivityLevelIs(Extreme);
    }

    @Test
    public void maximumExtremeVolume() {
        givenTotalSold(999999);

        whenMeasureVolume();

        thenMeasuredActivityLevelIs(Extreme);
    }

    @Test
    public void zeroPerformance() {
        givenShownAndSold(0, 0);

        whenMeasurePerformance();

        thenMeasuredActivityLevelIs(Insignificant);
    }

    @Test
    public void minimumInsignificantPerformance() {
        givenShownPerOneSold(999999);

        whenMeasurePerformance();

        thenMeasuredActivityLevelIs(Insignificant);
    }

    @Test
    public void maximumInsignificantPerformance() {
        givenShownPerOneSold(1000);

        whenMeasurePerformance();

        thenMeasuredActivityLevelIs(Insignificant);
    }

     @Test
    public void minimumElevatedPerformance() {
        givenShownPerOneSold(999);

        whenMeasurePerformance();

        thenMeasuredActivityLevelIs(Elevated);
    }

    @Test
    public void maximumElevatedPerformance() {
        givenShownPerOneSold(250);

        whenMeasurePerformance();

        thenMeasuredActivityLevelIs(Elevated);
    }

     @Test
    public void minimumHighPerformance() {
        givenShownPerOneSold(249);

        whenMeasurePerformance();

        thenMeasuredActivityLevelIs(High);
    }

    @Test
    public void maximumHighPerformance() {
        givenShownPerOneSold(100);

        whenMeasurePerformance();

        thenMeasuredActivityLevelIs(High);
    }

     @Test
    public void minimumExtremePerformance() {
        givenShownPerOneSold(99);

        whenMeasurePerformance();

        thenMeasuredActivityLevelIs(Extreme);
    }

    @Test
    public void maximumExtremePerformance() {
        givenShownPerOneSold(1);

        whenMeasurePerformance();

        thenMeasuredActivityLevelIs(Extreme);
    }

    private void givenShownAndSold(int shown, int sold) {
        asset = getAssetWith24HourTraffic(shown, sold);
    }

    private void givenShownPerOneSold(int shown) {
        givenShownAndSold(shown, 1);
    }

    private void givenTotalSold(int sold) {
        givenShownAndSold(0, sold);
    }
}
