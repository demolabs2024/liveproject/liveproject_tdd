package com.assetco.hotspots.optimization;

import com.assetco.search.results.*;

import static com.assetco.hotspots.optimization.ActivityLevel.*;
import static com.assetco.search.results.AssetVendorRelationshipLevel.*;
import static com.assetco.search.results.AssetVendorRelationshipLevel.Basic;
import static com.assetco.search.results.AssetVendorRelationshipLevel.Silver;

public class GoldGateBasedDealsApplicatorTests {
    public static class ForGoldAssets extends RelaxedRuleDealsApplicatorTests {

        @Override
        protected ActivityLevel getMinimumPerformance() {
            return Elevated;
        }

        @Override
        protected AssetVendorRelationshipLevel getAssetVendorLevel() {
            return Gold;
        }

        @Override
        protected ActivityLevel getDealEligibilityOverridePerformance() {
            return High;
        }

        @Override
        protected ActivityLevel getMinimumVolume() {
            return Elevated;
        }

        @Override
        protected DealsApplicator getApplicator() {
            return new GoldGateBasedDealsApplicator(assessments, measurements);
        }
    }

    public static class ForSilverAssets extends StrictRuleDealsApplicatorTests {

        @Override
        protected ActivityLevel getMinimumPerformance() {
            return High;
        }

        @Override
        protected AssetVendorRelationshipLevel getAssetVendorLevel() {
            return Silver;
        }

        @Override
        protected ActivityLevel getMinimumVolume() {
            return High;
        }

        @Override
        protected DealsApplicator getApplicator() {
            return new GoldGateBasedDealsApplicator(assessments, measurements);
        }
    }

    public static class ForBasicAssets extends ExcludedAssetsDealsApplicatorTests {
        @Override
        protected AssetVendorRelationshipLevel getExcludedCategoryOfVendors() {
            return Basic;
        }

        @Override
        protected DealsApplicator getApplicator() {
            return new GoldGateBasedDealsApplicator(assessments, measurements);
        }
    }
}
