package com.assetco.hotspots.optimization;

import com.assetco.search.results.Asset;

import static com.assetco.hotspots.optimization.ActivityLevel.*;

public class TrafficBasedAssetMeasurements implements AssetMeasurements{
    @Override
    public ActivityLevel getAssetPerformance(Asset asset) {
        var sold = asset.getPurchaseInfoLast24Hours().getTimesPurchased();
        if (sold == 0) {
            return Insignificant;
        }
        var shown = asset.getPurchaseInfoLast24Hours().getTimesShown();
        var shownPerSold = shown / sold;
        if (shownPerSold >= 1000) {
            return Insignificant;
        } else if (shownPerSold >= 250) {
            return Elevated;
        } else if (shownPerSold >= 100) {
            return High;
        }
        return Extreme;
    }

    @Override
    public ActivityLevel getAssetVolume(Asset asset) {
        var sold = asset.getPurchaseInfoLast24Hours().getTimesPurchased();
        if (sold <= 9) {
            return Insignificant;
        } else if (sold <= 19) {
            return Elevated;
        } else if (sold <= 99) {
            return High;
        }
        return Extreme;
    }
}
