package com.assetco.hotspots.optimization;

public abstract class ScoreBasedDealsApplicator extends DealsApplicator {
    protected final AssetDealsScoreCalculator scoreCalculator;

    public ScoreBasedDealsApplicator(AssetDealsScoreCalculator scoreCalculator) {
        this.scoreCalculator = scoreCalculator;
    }
}
