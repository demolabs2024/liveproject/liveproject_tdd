package com.assetco.hotspots.optimization;

import com.assetco.search.results.*;

// This class was our hint that there was such a thing as a gate-based applicator all along.
// It _never_ needed the measurements or assessments because it is an implementation of the
// Null Object pattern.
//
// Earlier refactorings weren't really done with enough understanding of the requirements to recognize this
// and it got missed in code-review.
//
// Since we were refactoring already, we cleaned this up while we were at it.
public class BasicDealsApplicator extends DealsApplicator {
    @Override
    public void applyDeals(SearchResults results, Asset asset) {
    }
}
