package com.assetco.hotspots.optimization;

import org.junit.jupiter.api.*;

import static com.assetco.search.results.AssetVendorRelationshipLevel.*;

// Again, here is a good example of the following maxim:
// Good factoring makes good tests.
//
// Each threshold is clearly delineated with a single test that shows what happens on each side of the
// associated boundary.
public class GoldScoreBasedDealsApplicatorTests extends ScoreBasedDealsApplicatorTests {

    private static final int goldThreshold = 16;
    private static final int silverThreshold = 32;

    @Override
    protected DealsApplicator getApplicator() {
        return new GoldScoreBasedDealsApplicator(scoreCalculator);
    }

    @Test
    public void goldAssetJustBelowThreshold() {
        scoringCase(Gold, goldThreshold - 1, false);
    }

    @Test
    public void goldAtThreshold() {
        scoringCase(Gold, goldThreshold, true);
    }

    @Test
    public void silverAssetJustBelowThreshold() {
        scoringCase(Silver, silverThreshold - 1, false);
    }

    @Test
    public void silverAtThreshold() {
        scoringCase(Silver, silverThreshold, true);
    }

    @Test
    public void basic() {
        scoringCase(Basic, 1000, false);
    }
}
