package com.assetco.hotspots.optimization;

import com.assetco.search.results.Asset;
import org.junit.jupiter.api.Assertions;

public class AssetMeasurementsTests {
    protected Asset asset;
    protected AssetMeasurements measurements;
    private ActivityLevel measured;

    protected void whenMeasurePerformance() {
        measured = measurements.getAssetPerformance(asset);
    }

    protected void thenMeasuredActivityLevelIs(ActivityLevel expected) {
        Assertions.assertEquals(expected, measured);
    }

    protected void whenMeasureVolume() {
        measured = measurements.getAssetVolume(asset);
    }
}
