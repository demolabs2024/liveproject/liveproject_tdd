package com.assetco.hotspots.optimization;

import com.assetco.search.results.*;
import org.junit.jupiter.api.*;

public abstract class ExcludedAssetsDealsApplicatorTests extends DealsApplicatorTests {
    @BeforeEach
    public void setup() {
        baseSetup();
        setGoverningRelationshipLevel(getApplicator());
    }

    protected abstract DealsApplicator getApplicator();

    @Test
    public void assetIsNotAdded() {
        givenAssetVendorLevel(getExcludedCategoryOfVendors());

        whenApplyDeals();

        thenAssetIsInDeals(false);
    }

    protected abstract AssetVendorRelationshipLevel getExcludedCategoryOfVendors();

}
