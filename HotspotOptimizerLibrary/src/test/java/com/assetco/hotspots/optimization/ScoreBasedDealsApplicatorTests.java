package com.assetco.hotspots.optimization;

import com.assetco.search.results.*;
import org.junit.jupiter.api.*;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public abstract class ScoreBasedDealsApplicatorTests extends DealsApplicatorTests {
    protected AssetDealsScoreCalculator scoreCalculator;

    @BeforeEach
    public void setup() {
        baseSetup();
        scoreCalculator = mock(AssetDealsScoreCalculator.class);
        applicator = getApplicator();
    }

    protected abstract DealsApplicator getApplicator();

    protected void scoringCase(AssetVendorRelationshipLevel level, int score, boolean expected) {
        givenAssetVendorLevel(level);
        givenAssetDealsScoreIs(score);

        whenApplyDeals();

        thenAssetIsInDeals(expected);
    }

    private void givenAssetDealsScoreIs(int score) {
        when(scoreCalculator.computeScoreFor(asset)).thenReturn(score);
    }
}
