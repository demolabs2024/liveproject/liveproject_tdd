package com.assetco.hotspots.optimization;

import com.assetco.search.results.*;
import org.junit.jupiter.api.*;

import static com.assetco.hotspots.optimization.ActivityLevel.*;
import static com.assetco.hotspots.optimization.AssetInfoFactory.*;
import static com.assetco.search.results.AssetVendorRelationshipLevel.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class AssetDealsScoreCalculatorTests {
    private AssetMeasurements measurements;
    private AssetDealsScoreCalculator scoreCalculator;
    private int score;
    private Asset asset;
    private AssetAssessments assessments;

    @BeforeEach
    public void setup() {
        measurements = mock(RevenueBasedAssetMeasurements.class);
        assessments = mock(AssetAssessments.class);
        scoreCalculator = new AssetDealsScoreCalculator(measurements, assessments);
        asset = getAsset(makeVendor(Basic));
        // Note that I randomize the default behaviors
        // This makes my tests stronger by ensuring they aren't accidentally coupled
        // to an assumption about how the mocked-out objects will behave.
        givenAssetPerformanceIs(anyActivityLevel());
        givenAssetVolumeIs(anyActivityLevel());
        givenAssetIsDealEligible(anyBooleanValue());
    }

    private void givenAssetIsDealEligible(boolean value) {
        when(assessments.isAssetDealEligible(asset)).thenReturn(value);
    }

    private void givenAssetVolumeIs(ActivityLevel value) {
        when(measurements.getAssetVolume(asset)).thenReturn(value);
    }

    private void givenAssetPerformanceIs(ActivityLevel value) {
        when(measurements.getAssetPerformance(asset)).thenReturn(value);
    }

    @Test
    public void baselineAssetScoreIsZero() {
        givenAssetVolumeIs(Insignificant);
        givenAssetPerformanceIs(Insignificant);
        givenAssetIsDealEligible(false);

        whenCalculateScore();

        thenScoreIs(0);
    }

    @Test
    public void performanceComponent() {
        specifyPerformanceIncrement(Elevated, 2);
        specifyPerformanceIncrement(High, 5);
        specifyPerformanceIncrement(Extreme, 30);
    }

    @Test
    public void volumeComponent() {
        specifyVolumeIncrement(Elevated, 1);
        specifyVolumeIncrement(High, 2);
        specifyVolumeIncrement(Extreme, 10);
    }

    @Test void eligibilityComponent() {
        givenAssetIsDealEligible(false);
        var baselineScore = givenScore();
        givenAssetIsDealEligible(true);

        whenCalculateScore();

        thenScoreIs(baselineScore + 15);
    }

    private void specifyVolumeIncrement(ActivityLevel volume, int expectedAmount) {
        givenAssetVolumeIs(Insignificant);
        var baselineScore = givenScore();
        givenAssetVolumeIs(volume);

        whenCalculateScore();

        thenScoreIs(baselineScore + expectedAmount);
    }

    private void specifyPerformanceIncrement(ActivityLevel performance, int expectedAmount) {
        givenAssetPerformanceIs(Insignificant);
        var baselineScore = givenScore();
        givenAssetPerformanceIs(performance);

        whenCalculateScore();

        thenScoreIs(baselineScore + expectedAmount);
    }

    private int givenScore() {
        return runScoreCalculation();
    }

    private void thenScoreIs(int expected) {
        assertEquals(expected, score);
    }

    private void whenCalculateScore() {
        score = runScoreCalculation();
    }

    private int runScoreCalculation() {
        return scoreCalculator.computeScoreFor(this.asset);
    }

}
