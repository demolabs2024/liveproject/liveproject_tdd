package com.assetco.hotspots.optimization;

import com.assetco.search.results.*;

import static com.assetco.search.results.AssetVendorRelationshipLevel.*;
import static com.assetco.search.results.HotspotKey.*;

public class SilverScoreBasedDealsApplicator extends ScoreBasedDealsApplicator {
    public SilverScoreBasedDealsApplicator(AssetDealsScoreCalculator scoreCalculator) {
        super(scoreCalculator);
    }

    @Override
    public void applyDeals(SearchResults results, Asset asset) {
        var score = scoreCalculator.computeScoreFor(asset);
        var level = asset.getVendor().getRelationshipLevel();

        if (level == Silver && score >= 20)
            results.getHotspot(Deals).addMember(asset);
    }
}
