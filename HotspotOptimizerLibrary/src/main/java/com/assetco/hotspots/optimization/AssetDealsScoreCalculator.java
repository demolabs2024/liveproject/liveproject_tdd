package com.assetco.hotspots.optimization;

import com.assetco.search.results.*;

// How could the design of this be improved?
public class AssetDealsScoreCalculator {
    private final AssetMeasurements measurements;
    private final AssetAssessments assessments;

    public AssetDealsScoreCalculator(AssetMeasurements measurements, AssetAssessments assessments) {
        this.measurements = measurements;
        this.assessments = assessments;
    }

    public int computeScoreFor(Asset asset) {
        int result = 0;
        switch (measurements.getAssetPerformance(asset)) {
            case Elevated: result += 2; break;
            case High: result += 5; break;
            case Extreme: result += 30; break;
        }

        switch (measurements.getAssetVolume(asset)) {
            case Elevated: result += 1; break;
            case High: result += 2; break;
            case Extreme: result += 10; break;
        }

        if (assessments.isAssetDealEligible(asset))
            result += 15;

        return result;
    }
}
