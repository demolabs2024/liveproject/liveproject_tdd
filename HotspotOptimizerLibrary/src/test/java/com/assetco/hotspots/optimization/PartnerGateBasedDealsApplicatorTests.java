package com.assetco.hotspots.optimization;

import com.assetco.search.results.*;
import org.junit.jupiter.api.*;

import static com.assetco.hotspots.optimization.ActivityLevel.*;
import static com.assetco.search.results.AssetVendorRelationshipLevel.*;

public class PartnerGateBasedDealsApplicatorTests extends DealsApplicatorTests {
    public static class ForBasicAssets extends ExcludedAssetsDealsApplicatorTests {
        @Override
        protected AssetVendorRelationshipLevel getExcludedCategoryOfVendors() {
            return Basic;
        }

        @Override
        protected DealsApplicator getApplicator() {
            return new PartnerGateBasedDealsApplicator(assessments, measurements);
        }
    }

    public static class ForSilverAssets extends StrictRuleDealsApplicatorTests {

        @Override
        protected ActivityLevel getMinimumPerformance() {
            return Extreme;
        }

        @Override
        protected ActivityLevel getMinimumVolume() {
            return Extreme;
        }

        @Override
        protected AssetVendorRelationshipLevel getAssetVendorLevel() {
            return Silver;
        }

        @Override
        protected DealsApplicator getApplicator() {
            return new PartnerGateBasedDealsApplicator(assessments, measurements);
        }
    }

    public static class ForGoldAssets extends StrictRuleDealsApplicatorTests {
        @Override
        protected ActivityLevel getMinimumPerformance() {
            return High;
        }

        @Override
        protected ActivityLevel getMinimumVolume() {
            return Elevated;
        }

        @Override
        protected AssetVendorRelationshipLevel getAssetVendorLevel() {
            return Gold;
        }

        @Override
        protected DealsApplicator getApplicator() {
            return new PartnerGateBasedDealsApplicator(assessments, measurements);
        }
    }

    @Nested
    class ForPartnerAssets extends DealsApplicatorTests {
        @BeforeEach
        public void setup() {
            baseSetup();
            setGoverningRelationshipLevel(new PartnerGateBasedDealsApplicator(assessments, measurements));
            givenAssetVendorLevel(Partner);
        }

        @Test
        public void elevatedPerformance() {
            givenPerformance(Elevated);

            whenApplyDeals();

            thenAssetIsInDeals(true);
        }

        @Test
        public void insignificantPerformance() {
            givenPerformance(Insignificant);

            whenApplyDeals();

            thenAssetIsInDeals(false);
        }
    }
}
