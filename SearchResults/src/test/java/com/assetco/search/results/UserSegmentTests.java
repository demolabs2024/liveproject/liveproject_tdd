package com.assetco.search.results;

import org.junit.jupiter.api.*;

import java.util.*;
import java.util.stream.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UserSegmentTests {
    // The correct answer to the question "Why test this?" is "That's the wrong question."
    // The assumption is that something will be tested.
    // The question that we use to decide what to test and what not to test is "Why should I NOT test this?"
    //   - The cost is low, so we can't disqualify on cost vs. reward.
    //   - This is a direct reflection of the requirements as we know them, so we can't disqualify as an
    //     implementation detail
    @Test
    public void userSegmentValues() {
        assertEquals(
                namesOfUserSegmentEnumerationValues(),
                Arrays.asList("NewsMedia", "OtherMedia", "GeneralPublic"));
    }

    private List<String> namesOfUserSegmentEnumerationValues() {
        return Arrays.stream(UserSegment.values()).map(Enum::name).collect(Collectors.toList());
    }
}
