package com.assetco.hotspots.optimization;

import com.assetco.search.results.*;

import static com.assetco.search.results.AssetVendorRelationshipLevel.*;
import static com.assetco.search.results.HotspotKey.*;

public class GoldScoreBasedDealsApplicator extends ScoreBasedDealsApplicator {
    public GoldScoreBasedDealsApplicator(AssetDealsScoreCalculator scoreCalculator) {
        super(scoreCalculator);
    }

    @Override
    public void applyDeals(SearchResults results, Asset asset) {
        var score = scoreCalculator.computeScoreFor(asset);
        var vendorLevel = asset.getVendor().getRelationshipLevel();

        boolean add = (vendorLevel == Gold && score >= 16) || (vendorLevel == Silver && score >= 32);

        if (add)
            results.getHotspot(Deals).addMember(asset);
    }
}
