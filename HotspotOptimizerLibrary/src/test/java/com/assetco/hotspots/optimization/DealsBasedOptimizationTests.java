package com.assetco.hotspots.optimization;

import com.assetco.search.results.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.*;
import org.junit.jupiter.params.provider.*;

import static com.assetco.search.results.HotspotKey.*;

public class DealsBasedOptimizationTests extends OptimizerTests {
    // Search results with partner-level assets in them should work EXACTLY the same way for
    // both NewsMedia (the old default) and OtherMedia. So no need to change this test.

    @Test
    public void partnerLevelDealsForGeneralPublic() {
        givenUserSegment(UserSegment.GeneralPublic);
        var partnerLevelIn = givenAssetWith24DayShownAndSold(partnerVendor,999, 1, false);
        var partnerLevelOut = givenAssetWith24DayShownAndSold(partnerVendor, 1000, 1, false);
        var goldLevelIn = givenAssetWith24DayShownAndSold(goldVendor,2499, 10, true);
        var goldLevelOut = givenAssetWith24DayShownAndSold(goldVendor, 2500, 10, false);
        var silverLevelIn = givenAssetWith24DayShownAndSold(silverVendor,9999, 100, true);
        var silverLevelOut = givenAssetWith24DayShownAndSold(silverVendor, 10000, 100, true);
        var basicLevel = givenAssetWith24DayShownAndSold(basicVendor, 1000, 1000, true);

        whenOptimize();

        thenHotspotHas(Deals, partnerLevelIn, goldLevelIn, silverLevelIn);
        thenHotspotDoesNotHave(Deals, partnerLevelOut, goldLevelOut, silverLevelOut, basicLevel);
    }

    @ParameterizedTest
    @EnumSource(value = UserSegment.class, names = {"NewsMedia", "OtherMedia"}, mode = EnumSource.Mode.INCLUDE)
    public void partnerLevelDeals(UserSegment segment) {
        givenUserSegment(segment);
        var partnerLevel = givenAssetWith30DayProfitabilityAndDealEligibility(partnerVendor, "700", "1000", false);
        var goldLevelIn = givenAssetWith30DayProfitabilityAndDealEligibility(goldVendor, "500", "1000", true);
        var goldLevelOut = givenAssetWith30DayProfitabilityAndDealEligibility(goldVendor, "500", "1000", false);
        var silverLevelIn = givenAssetWith30DayProfitabilityAndDealEligibility(silverVendor, "2500", "10000", true);
        var silverLevelOut = givenAssetWith30DayProfitabilityAndDealEligibility(silverVendor, "2500.1", "10000", true);
        var basicLevel = givenAssetWith30DayProfitabilityAndDealEligibility(basicVendor, "0", "100000", true);

        whenOptimize();

        thenHotspotHas(Deals, partnerLevel, goldLevelIn, silverLevelIn);
        thenHotspotDoesNotHave(Deals, goldLevelOut, silverLevelOut, basicLevel);
    }

    @Test
    public void goldLevelDealsForGeneralPublic() {
        givenUserSegment(UserSegment.GeneralPublic);
        var goldLevelIn = givenAssetWith24DayShownAndSold(goldVendor,9999, 10, true);
        var goldLevelOut = givenAssetWith24DayShownAndSold(goldVendor, 19, 9, true);
        var silverLevelIn = givenAssetWith24DayShownAndSold(silverVendor,24749, 99, true);
        var silverLevelOut = givenAssetWith24DayShownAndSold(silverVendor, 24750, 99, true);
        var basicLevel = givenAssetWith24DayShownAndSold(basicVendor, 100, 100, true);

        whenOptimize();

        thenHotspotHas(Deals, goldLevelIn, silverLevelIn);
        thenHotspotDoesNotHave(Deals, goldLevelOut, silverLevelOut, basicLevel);
    }

    @Test
    public void goldLevelDealsForNewsMediaSegment() {
        givenUserSegment(UserSegment.NewsMedia);
        var goldLevelIn = givenAssetWith30DayProfitabilityAndDealEligibility(goldVendor, "700", "1000", true);
        var goldLevelOut = givenAssetWith30DayProfitabilityAndDealEligibility(goldVendor, "0", "999.99", true);
        var silverLevelIn = givenAssetWith30DayProfitabilityAndDealEligibility(silverVendor, "750", "1500", true);
        var silverLevelOut = givenAssetWith30DayProfitabilityAndDealEligibility(silverVendor, "750", "1500", false);
        var basicLevel = givenAssetWith30DayProfitabilityAndDealEligibility(basicVendor, "0", "100000", true);

        whenOptimize();

        thenHotspotHas(Deals, goldLevelIn, silverLevelIn);
        thenHotspotDoesNotHave(Deals, goldLevelOut, silverLevelOut, basicLevel);
    }

    // Added this test to drive integration of the new functionality when Gold is the highest-level vendor
    // represented in a results set.
    @Test
    public void goldLevelDealsForOtherMediaSegment() {
        givenUserSegment(UserSegment.OtherMedia);
        var goldLevelIn = givenAssetWith30DayProfitabilityAndDealEligibility(goldVendor, "1000", "1000", true);
        var goldLevelOut = givenAssetWith30DayProfitabilityAndDealEligibility(goldVendor, "999.99", "999.99", true);
        var silverLevelIn = givenAssetWith30DayProfitabilityAndDealEligibility(silverVendor, "0", "1000", true);
        var silverLevelOut = givenAssetWith30DayProfitabilityAndDealEligibility(silverVendor, "1", "1", false);
        var basicLevel = givenAssetWith30DayProfitabilityAndDealEligibility(basicVendor, "0", "100000", true);

        whenOptimize();

        thenHotspotHas(Deals, goldLevelIn, silverLevelIn);
        thenHotspotDoesNotHave(Deals, goldLevelOut, silverLevelOut, basicLevel);
    }

    @Test
    public void silverLevelDealsForGeneralPublic() {
        givenUserSegment(UserSegment.GeneralPublic);
        var silverLevelIn = givenAssetWith24DayShownAndSold(silverVendor,4999, 20, false);
        var silverLevelOut = givenAssetWith24DayShownAndSold(silverVendor, 5000, 19, false);
        var basicLevel = givenAssetWith24DayShownAndSold(basicVendor, 100, 100, true);

        whenOptimize();

        thenHotspotHas(Deals, silverLevelIn);
        thenHotspotDoesNotHave(Deals, silverLevelOut, basicLevel);
    }

    @Test
    public void silverLevelDealsForNewsMediaSegment() {
        givenUserSegment(UserSegment.NewsMedia);
        var silverLevelIn = givenAssetWith30DayProfitabilityAndDealEligibility(silverVendor, "750", "1500", true);
        var silverLevelOut = givenAssetWith30DayProfitabilityAndDealEligibility(silverVendor, "1050", "1500", false);
        var basicLevel = givenAssetWith30DayProfitabilityAndDealEligibility(basicVendor, "0", "100000", true);

        whenOptimize();

        thenHotspotHas(Deals, silverLevelIn);
        thenHotspotDoesNotHave(Deals, silverLevelOut, basicLevel);
    }

    // Added this one for silver/OtherMedia.
    @Test
    public void silverLevelDealsForOtherMediaSegment() {
        givenUserSegment(UserSegment.OtherMedia);
        var silverLevelIn = givenAssetWith30DayProfitabilityAndDealEligibility(silverVendor, "50", "100", true);
        var silverLevelOut = givenAssetWith30DayProfitabilityAndDealEligibility(silverVendor, "50", "50", false);
        var basicLevel = givenAssetWith30DayProfitabilityAndDealEligibility(basicVendor, "0", "100000", true);

        whenOptimize();

        thenHotspotHas(Deals, silverLevelIn);
        thenHotspotDoesNotHave(Deals, silverLevelOut, basicLevel);
    }
}
