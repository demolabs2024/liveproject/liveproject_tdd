package com.assetco.hotspots.optimization;

import org.junit.jupiter.api.*;

import static com.assetco.search.results.AssetVendorRelationshipLevel.*;

public class SilverScoreBasedDealsApplicatorTests extends ScoreBasedDealsApplicatorTests {
    static final int silverThreshold = 20;

    @Override
    protected DealsApplicator getApplicator() {
        return new SilverScoreBasedDealsApplicator(scoreCalculator);
    }

    @Test
    public void silverJustBelowThreshold() {
        scoringCase(Silver, silverThreshold - 1, false);
    }

    @Test
    public void silverAtThreshold() {
        scoringCase(Silver, silverThreshold, true);
    }

    @Test
    public void basic() {
        scoringCase(Basic, 10000, false);
    }
}
