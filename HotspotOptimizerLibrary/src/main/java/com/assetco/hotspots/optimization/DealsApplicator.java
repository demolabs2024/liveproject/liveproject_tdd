package com.assetco.hotspots.optimization;

import com.assetco.search.results.*;

import static com.assetco.search.results.UserSegment.*;

public abstract class DealsApplicator {
    public static DealsApplicator createInstance(
            AssetVendorRelationshipLevel highestRelationshipLevel,
            UserSegment userSegment,
            AssetAssessments assessments) {
        var measurements = AssetMeasurements.createInstance(userSegment);
        if (highestRelationshipLevel == AssetVendorRelationshipLevel.Partner)
            return new PartnerGateBasedDealsApplicator(assessments, measurements);

        if (userSegment == OtherMedia)
            return createOtherMediaInstance(highestRelationshipLevel, assessments, measurements);

        return createNewsMediaInstance(highestRelationshipLevel, assessments, measurements);
    }

    private static DealsApplicator createOtherMediaInstance(AssetVendorRelationshipLevel highestRelationshipLevel, AssetAssessments assessments, AssetMeasurements measurements) {
        var scoring = new AssetDealsScoreCalculator(measurements, assessments);

        switch (highestRelationshipLevel) {
            case Gold:
                return new GoldScoreBasedDealsApplicator(scoring);
            case Silver:
                return new SilverScoreBasedDealsApplicator(scoring);
        }

        return new BasicDealsApplicator();
    }

    private static DealsApplicator createNewsMediaInstance(AssetVendorRelationshipLevel highestRelationshipLevel, AssetAssessments assessments, AssetMeasurements measurements) {
        switch (highestRelationshipLevel) {
            case Gold:
                return new GoldGateBasedDealsApplicator(assessments, measurements);
            case Silver:
                return new SilverGateBasedDealsApplicator(assessments, measurements);
        }

        return new BasicDealsApplicator();
    }

    public abstract void applyDeals(SearchResults results, Asset asset);
}
