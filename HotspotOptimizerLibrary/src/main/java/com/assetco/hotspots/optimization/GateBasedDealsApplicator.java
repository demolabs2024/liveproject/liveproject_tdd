package com.assetco.hotspots.optimization;

public abstract class GateBasedDealsApplicator extends DealsApplicator {

    protected final AssetAssessments assessments;
    protected final AssetMeasurements measurements;

    public GateBasedDealsApplicator(AssetAssessments assessments, AssetMeasurements measurements) {
        super();
        this.assessments = assessments;
        this.measurements = measurements;
    }
}
