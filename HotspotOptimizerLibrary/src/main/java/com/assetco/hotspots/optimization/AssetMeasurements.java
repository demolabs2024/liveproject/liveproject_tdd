package com.assetco.hotspots.optimization;

import com.assetco.search.results.Asset;
import com.assetco.search.results.UserSegment;

public interface AssetMeasurements {
    static AssetMeasurements createInstance(UserSegment userSegment) {
        switch (userSegment) {
            case NewsMedia:
            case OtherMedia:
                return new RevenueBasedAssetMeasurements();
            case GeneralPublic:
                return new TrafficBasedAssetMeasurements();
        }
        return null;
    }

    ActivityLevel getAssetPerformance(Asset asset);

    // As our understanding of the problem improves, so should our design
    // "volume" is a better term than "traffic", so I renamed this method.
    ActivityLevel getAssetVolume(Asset asset);
}
