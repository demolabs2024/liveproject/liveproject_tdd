package com.assetco.hotspots.optimization;

import com.assetco.search.results.*;

import static com.assetco.hotspots.optimization.ActivityLevel.*;
import static com.assetco.search.results.AssetVendorRelationshipLevel.*;

public class SilverGateBasedDealsApplicatorTests extends DealsApplicatorTests {
    public static class ForBasicAssets extends ExcludedAssetsDealsApplicatorTests {
        @Override
        protected AssetVendorRelationshipLevel getExcludedCategoryOfVendors() {
            return Basic;
        }

        @Override
        protected DealsApplicator getApplicator() {
            return new SilverGateBasedDealsApplicator(assessments, measurements);
        }
    }

    public static class ForSilverAssets extends RelaxedRuleDealsApplicatorTests {

        @Override
        protected AssetVendorRelationshipLevel getAssetVendorLevel() {
            return Silver;
        }

        @Override
        protected ActivityLevel getMinimumPerformance() {
            return Elevated;
        }

        @Override
        protected ActivityLevel getDealEligibilityOverridePerformance() {
            return High;
        }

        @Override
        protected ActivityLevel getMinimumVolume() {
            return High;
        }

        @Override
        protected DealsApplicator getApplicator() {
            return new SilverGateBasedDealsApplicator(assessments, measurements);
        }
    }
}
